#!/bin/bash
curdir=$(pwd)
dl_url="https://hosts.gameindustry.eu/pc-main-template/dl/"
list_name="tracking-games.txt"
rm -f games.list
wget -qO- "$dl_url" > games.list
sed -i '/google/Id' games.list
sed -i '/APPLE/Id' games.list
sed -i '/Facebook/Id' games.list
sed -i '/Microsoft/Id' games.list
sed -i '/Twitter/Id' games.list
sed -i '/OCSP/Id' games.list
sed -i '/CRL/Id' games.list
sed -i '/CDN/Id' games.list
sed -i '/127/d' games.list
sed -i '/255.255/d' games.list
sed -i '/::1/d' games.list
sed -i '/ff00:/d' games.list
sed -i '/ff02:/d' games.list
sed -i '/fe80:/d' games.list
sed -i '/fe00:/d' games.list
 
