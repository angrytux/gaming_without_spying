# gaming_without_spying

##### Blocklist against spyware, tracking and analytics in games.

Tracking and analyzing users doesn't stop when it comes to gaming.

On **[gameindustry](https://gameindustry.eu/)** you will find an unbelievably large list which opposes this.

You can grab the whole blocklist (hosts file) from this site. Alternately I provide a slightly modified list here. The reason for was making it integrateable as addition for other platforms and tools. 

_Modifications:_

Entries that contain Google,Microsoft,Apple,Facebook,Twitter,OCSP,CRL,CDNs were removed. Else online services could be severely limited. There are a many other blocklists out there for trackers and ads in general. 

Modified blocklist:

`https://codeberg.org/angrytux/gaming_without_spying/raw/branch/master/tracking-games.txt`


I only use a simple script and offer the finished file. Updates are made regularly when changes are made.

### **All the hard work and testing is done by the awesome guy from [gameindustry](https://gameindustry.eu/)**

[<img src="https://gameindustry.eu/images/header/logo_2024.png">](https://gameindustry.eu/)


**License:** [CC BY-NC-SA 4.0](https://gameindustry.eu/u/lizenz/)

